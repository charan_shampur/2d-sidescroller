﻿using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour {
	public PlayerMovement1 Player_health;
	public EnemyMovementOpp Enemy_health;
	public bool enemy=false;
	public Transform ForegroundSprite;
	public SpriteRenderer ForegroundRenderer;
	public Color MaxHealthColor = new Color(255 / 255f, 63 / 255f, 63 /255f);
	public Color MinHealthColor = new Color(64 / 255f, 137 / 255f, 255 /255f);


	// Use this for initialization
	void Start () {
		if(!enemy)
			Player_health = FindObjectOfType<PlayerMovement1>();
		else
			Enemy_health = FindObjectOfType<EnemyMovementOpp>();
		//Player_health.animator.SetInteger ("AnimState", 0);
		}
	
	// Update is called once per frame
	void Update () {
		if (!enemy) {
						var healthPercent = Player_health.health / (float)Player_health.Maxhealth;
						ForegroundSprite.localScale = new Vector3 (healthPercent, 1, 1);
						ForegroundRenderer.color = Color.Lerp (MaxHealthColor, MinHealthColor, healthPercent);
				} else {
			var healthPercent = Enemy_health.health / (float)Enemy_health.Maxhealth;
			ForegroundSprite.localScale = new Vector3 (healthPercent, 1, 1);
			ForegroundRenderer.color = Color.Lerp (MaxHealthColor, MinHealthColor, healthPercent);
				}
		}

	public void destroy (){
		Destroy (this.gameObject);
		}
	
}