﻿using UnityEngine;
using System.Collections;

public class shoot : MonoBehaviour {

	public GameObject bullet;
	public Vector2 positionLeft;
	public Vector2 positionRight;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		positionLeft = new Vector2 (transform.position.x-0.7f,transform.position.y+.45f);
		positionRight = new Vector2 (transform.position.x+0.7f,transform.position.y+.45f);
		if (PlayerMovement1.fireActive)
		{
			if(PlayerMovement1.cinnamonFire)
			{
				if(!PlayerMovement1.facingRight)
				{
					shootLeft();
				}
				else if(PlayerMovement1	.facingRight)
				{
					shootRight();
				}
			}
		}
	
	}

	void shootLeft()
	{
		Instantiate(bullet,positionLeft,Quaternion.identity);
		PlayerMovement1.cinnamonFire = false;
	}

	void shootRight()
	{
		Instantiate(bullet, positionRight, Quaternion.identity);
		PlayerMovement1.cinnamonFire = false;
	}
}
