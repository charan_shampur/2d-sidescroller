﻿using UnityEngine;
using System.Collections;

public class BulletForce : MonoBehaviour {

	// Use this for initialization
	public int speed;
	public bool OnlyGoLeft = false;
	public bool OnlyGoRight = false;
	public GameObject explosion;
	void Start () {
		if(!PlayerMovement1.facingRight)
		{
			OnlyGoLeft = true;
		}
		if(PlayerMovement1.facingRight)
		{
			OnlyGoRight = true;
		}
	
	}
	
	// Updae is called once per frame
	void Update () {
		if (OnlyGoLeft == true)
		{
			GetComponent<Rigidbody2D>().AddForce(Vector3.left * speed);
		}
		
		if (OnlyGoRight == true)
		{
			GetComponent<Rigidbody2D>().AddForce(Vector3.right * speed);
		}
	
	}

	void OnCollisionEnter2D(Collision2D other )
	{
		//Debug.Log ("bullet Colider");

		if(other.gameObject.tag != "Player"){
			if (explosion!=null)
			{
				Instantiate (explosion, transform.position, transform.rotation);
			}
			if (other.gameObject.tag == "StagnentEnemy")
			{
				other.gameObject.SendMessage("takeDamage");
			}
			if (other.gameObject.tag == "Enemy")
			{
				other.gameObject.SendMessage("takeDamage");
			}
			if (other.gameObject.tag == "turretAmmo")
			{
				other.gameObject.SendMessage("takeDamage");
			}
			Destroy(this.gameObject);
		}
	}

	void FixedUpdate()
	{
		//Instantiate (explosion, transform.position, transform.rotation);
		//Destroy (this.explosion);
		Destroy (this.gameObject, 1.5f);

		}

}
