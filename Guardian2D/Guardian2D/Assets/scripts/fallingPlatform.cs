﻿using UnityEngine;
using System.Collections;

public class fallingPlatform : MonoBehaviour {

	public bool falling=false;
	public Vector2 initPos = new Vector2();
	public Vector2 origPos = new Vector2();
	//public GameObject explosion;
	// Use this for initialization
	void Start () {
		origPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {

		if(falling)
		{
			initPos = transform.position;
			initPos.y -= .02f;
			transform.position = initPos;

		}
	
	}

	void OnCollisionEnter2D(Collision2D other )
	{
		//Debug.Log ("falling platform Colider");
		if(other.gameObject.tag == "Player"){
			falling=true;
			
			//GameObject obj = GameObject.Find("doorOpen");
			//obj.SendMessage("startClose");
			//Destroy(this.gameObject);
			
		}
	}
}
