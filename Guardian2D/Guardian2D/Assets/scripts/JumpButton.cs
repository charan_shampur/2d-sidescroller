﻿/*/
* Script by Devin Curry
* www.Devination.com
* www.youtube.com/user/curryboy001
* Please like and subscribe if you found my tutorials helpful :D
* Google+ Community: https://plus.google.com/communities/108584850180626452949
* Twitter: https://twitter.com/Devination3D
* Facebook Page: https://www.facebook.com/unity3Dtutorialsbydevin
/*/
using UnityEngine;
using System.Collections;

public class JumpButton : TouchLogicV2 
{
	PlayerMovement1 player;
	//public bool jumpPressed=false;
	void Start()
	{
		player = FindObjectOfType<PlayerMovement1>();
		//player.animator.SetInteger ("AnimState", 0);
	}

	 public override void OnTouchStayed()
	{

		if (player.flying) {
						player.jump ();
			//jumpPressed=true;
			//player.jumpTouch=true;
				}

	}
    
	public override void OnTouchBegan()
	{
		if (!player.fired) {
						player.jump ();
				}
	}

	public override void OnTouchEndedAnywhere()
	{
		/*
		if(jumpPressed)
		{
			player.jumpTouch=false;
			jumpPressed=false;
		} */
	}
	

	
}