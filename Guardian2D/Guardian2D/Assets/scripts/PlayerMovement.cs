﻿/*/
* Script by Devin Curry
* www.Devination.com
* www.youtube.com/user/curryboy001
* Please like and subscribe if you found my tutorials helpful :D
* Google+ Community: https://plus.google.com/communities/108584850180626452949
* Twitter: https://twitter.com/Devination3D
* Facebook Page: https://www.facebook.com/unity3Dtutorialsbydevin
/*/
using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour 
{
	public float speed = 500f, jumpHeight = 500f;
	Transform myTrans;
	Rigidbody2D myBody;
	Vector2 movement;//used as temp holder variable before applying to velocity
	bool isGrounded = true;


	void Start () 
	{
		myTrans = this.transform;
		myBody = this.GetComponent<Rigidbody2D>();
	}

	void Update () 
	{
		//Check if the player is grounded or not
		/*isGrounded = Physics2D.Linecast(myTrans.position,
		                                GameObject.Find(this.name+"/tag_ground").transform.position,
		                                LayerMask.NameToLayer("Player"));

		//Just for visualization, not needed for gameplay
		Debug.DrawLine (myTrans.position,
		                GameObject.Find(this.name+"/tag_ground").transform.position,
		                Color.red);
		*/
		
		//Works for keyboards and joysticks
		#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY && !UNITY_WINRT
		Move (Input.GetAxisRaw("Horizontal"));
		if (Input.GetButtonDown ("Jump"))
			Jump ();
		#endif
	}
	//
	//Separate Move and Jump so they can be called externally by TouchButtons
	//
	public void Move(float horizontal_input)
	{
		movement = myBody.velocity;

		if(isGrounded)//we can only move left and right if on the ground
			movement.x = horizontal_input * speed * Time.deltaTime;

		//Apply the movement to the player
		myBody.velocity = movement;
	}
	public void Jump()//we can only jump if on the ground
	{
		if(isGrounded)
			myBody.velocity += jumpHeight * Vector2.up * Time.deltaTime;
	}
}