﻿using UnityEngine;
using System.Collections;

public class EnemyMelee : MonoBehaviour {
	
	void OnTriggerEnter(Collider other )
	{
		//Debug.Log ("destroying enemy from EnemyMelee");

		if(other.gameObject.tag == "Player"){
			Destroy(gameObject);
		}
	}
}