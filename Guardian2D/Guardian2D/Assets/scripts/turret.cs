﻿using UnityEngine;
using System.Collections;

public class turret : MonoBehaviour {

	// Use this for initialization
	public int maxhealth = 100;
	public GameObject explosion;
	public bool lightning=false;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(maxhealth < 0)
		{
			Instantiate(explosion,transform.position,transform.rotation);
			if(lightning)
			{
				GameObject lightObj = GameObject.Find("lightning1");
				lightObj.SendMessage("DestroyLight");
			}
			Destroy(gameObject);
		}
	
	}

	public void takeDamage()
	{
		maxhealth -= 25;
	}

	void OnCollisionEnter2D(Collision2D other )
	{
		//Debug.Log ("turret Colider");
		if(other.gameObject.tag == "Player"){
			
			//other.gameObject.SendMessage("activateShield");
			if(lightning)
			{
				other.gameObject.SendMessage("death");
			}
			else
			{
				other.gameObject.SendMessage("takeDamage");
			}
			//Destroy(this.gameObject);
			
		}
	}

	void OnCollisionStay2D(Collision2D other )
	{
		//Debug.Log ("turret Colider");
		if(other.gameObject.tag == "Player"){
			
			//other.gameObject.SendMessage("activateShield");
			if(lightning)
			{
				other.gameObject.SendMessage("death");
			}
			else
			{
				other.gameObject.SendMessage("takeDamage");
			}
			//Destroy(this.gameObject);
			
		}
	}


}
