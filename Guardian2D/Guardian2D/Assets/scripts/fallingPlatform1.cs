﻿using UnityEngine;
using System.Collections;

public class fallingPlatform1 : MonoBehaviour {

	// Use this for initialization
	Vector2 origPos = new Vector2();
	void Start () {
		origPos = transform.position;
		gameObject.SetActive (true);
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ResetPlatform()
	{
		transform.position = origPos;
		gameObject.SetActive (true);

	}
}
