﻿using UnityEngine;
using System.Collections;

public class SimpleEnemyScripy : MonoBehaviour {
	
	public float velocity = 1f;
	
	public Transform sightStart;
	public Transform sightEnd;
	
	public LayerMask detectWhat;
	
	//public Transform weakness;
	
	public bool colliding;
	public int maxhealth = 100;
	public GameObject explosion1,explosion2;
	
	//Animator anim;
	
	
	
	
	// Use this for initialization
	void Start () {
		//anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		
		GetComponent<Rigidbody2D>().velocity = new Vector2 (velocity, GetComponent<Rigidbody2D>().velocity.y);
		
		colliding = Physics2D.Linecast (sightStart.position, sightEnd.position, detectWhat);
		
		if (colliding) {
			
			transform.localScale = new Vector2 (transform.localScale.x * -1, transform.localScale.y);
			velocity*= -1;
			
		}
		if(maxhealth < 0)
		{
			Instantiate(explosion1,transform.position,transform.rotation);
			Destroy(gameObject);
		}
		
	}
	
	void OnDrawGizmos()
	{
		Gizmos.color = Color.magenta;
		
		Gizmos.DrawLine (sightStart.position, sightEnd.position);
	}
	
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Player") {
			
			other.gameObject.SendMessage ("takeDamage");
			ContactPoint2D contact = other.contacts [0];
			Quaternion rot = Quaternion.FromToRotation (Vector2.up, contact.normal);
			Vector2 explosionPoint = contact.point;
			Instantiate (explosion2, explosionPoint, rot);
			Vector2 vec = other.gameObject.transform.position;
			vec.x = vec.x - .8f;
			other.gameObject.transform.position = vec;
			
		}
		if (other.gameObject.tag == "turretAmmo")
		{
			transform.localScale = new Vector2 (transform.localScale.x * -1, transform.localScale.y);
			velocity*= -1;
		}
	}

	void OnCollisionStay2D(Collision2D other)
	{
		if (other.gameObject.tag == "Player") {
			
			other.gameObject.SendMessage ("takeDamage");
			ContactPoint2D contact = other.contacts [0];
			Quaternion rot = Quaternion.FromToRotation (Vector2.up, contact.normal);
			Vector2 explosionPoint = contact.point;
			Instantiate (explosion2, explosionPoint, rot);
			Vector2 vec = other.gameObject.transform.position;
			vec.x = vec.x - .8f;
			other.gameObject.transform.position = vec;
			
		}
		if (other.gameObject.tag == "turretAmmo")
		{
			transform.localScale = new Vector2 (transform.localScale.x * -1, transform.localScale.y);
			velocity*= -1;
		}
	}
	
	public void takeDamage()
	{
		maxhealth -= 20;
	}
	
	
	
	
	
	
	
	
	
	
	
}
