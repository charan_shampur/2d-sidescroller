﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {
	StagnentEnemy enemy;
	PlayerMovement1 player;
	private bool leftflag;
	private bool rightflag;
	public GameObject explosion;
	// Use this for initialization
	void Start () {
		enemy = FindObjectOfType<StagnentEnemy>();
		if (enemy.ans < 0) {
			leftflag = true;
		} else {
			rightflag = true;
		}
	
	}
	// Update is called once per frame
	void Update () {
		//Debug.Log (enemy.ans);
		if (leftflag){
			GetComponent<Rigidbody2D>().AddForce (new Vector2 (-.8f, 0));
		} 
		if(rightflag){
			GetComponent<Rigidbody2D>().AddForce (new Vector2 (.8f, 0));
			Vector2 vect = new Vector2(0.4f, 0.4f);
			this.transform.localScale = vect;
		}
	}

	void OnCollisionEnter2D(Collision2D target) {
		if(target.gameObject.tag != "StagnentEnemy"){
			if(target.gameObject.tag == "Player")
			{
			target.gameObject.SendMessage("takeDamage");
			}
			Instantiate(explosion,transform.position,transform.rotation);
			Destroy(gameObject);

		}
    }
}
