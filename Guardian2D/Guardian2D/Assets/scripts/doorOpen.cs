﻿using UnityEngine;
using System.Collections;

public class doorOpen : MonoBehaviour {

	// Use this for initialization
	public Vector2 initPos = new Vector2();
	public Vector2 origPos = new Vector2();
	public bool triggeredOpen = false, triggeredClose = false;
	public float yPos;
	void Start () {
		//yPos = 1.5f;
		origPos = transform.position;
	
	}
	
	// Update is called once per frame
	void Update () {

		if(triggeredOpen)
		{
			if(transform.position.y<yPos)
			{
				initPos = transform.position;
				initPos.y += .03f;
				transform.position = initPos;
			}
		}

		if(triggeredClose)
		{
			if(transform.position.y > origPos.y)
			{
				initPos = transform.position;
				initPos.y -= .03f;
				transform.position = initPos;
			}
		}

	
	}

	public void startOpen()
	{
		triggeredOpen = true;
		triggeredClose = false;
	}

	public void startClose()
	{
		triggeredOpen = false;
		triggeredClose = true;
	}
}
