﻿using UnityEngine;
using System.Collections;

public class StagnentEnemy : MonoBehaviour {
	public GameObject player;
	public Animator animator;
	public bool lookingRight;
	public Transform knife;
	Vector3 r;
	public float MaxDist = 4.0f;
	public float speed = 10f;
	private bool readytoAttack;
	public float attackDelay = 3.0f;
	public Projectile projectile;
	public bool flag = false;
	public float ans;
	public int MaxHealth = 100;
	// Use this for initialization
	void Start () {
		lookingRight = true;
		r = transform.right * 1;
		animator = GetComponent<Animator> ();
		animator.SetInteger ("throwKnife", 0);

		if (attackDelay > 0) {

			StartCoroutine(OnAttack());
		}
		MaxHealth = 100;
		
	}
	
	// Update is called once per frame
	void Update () {


		Vector3 t = player.transform.position - transform.position;
		//Transform clone = Instantiate (knife) as Transform;
		animator.SetInteger ("throwKnife", 0);
		t.Normalize ();
		ans = Vector3.Dot (r, t);
		if(ans < 0 && lookingRight) {
			Vector3 newpos = this.transform.position;
			newpos.z -=1;
			lookingRight = false;
			this.transform.LookAt(newpos);
			
		}
		if (ans > 0 && !lookingRight) {
			Vector3 newpos = this.transform.position;
			newpos.z += 1;
			lookingRight = true;
			this.transform.LookAt (newpos);
		}
		Vector2 pos = transform.position;
		if (lookingRight) {
			pos.x += .32f;
			pos.y += .42f;
		} else {
			pos.x -= .32f;
			pos.y += .42f;
		}
		Vector2 vect = new Vector2 (-0.4f, 0.4f);
		//clone.position = pos;
		//clone.localScale = vect;
		if (MaxHealth < 0) {
			die();
				}
	}

	IEnumerator OnAttack(){

		yield return new WaitForSeconds (attackDelay);
		Fire ();
		StartCoroutine (OnAttack ());
	}

	void Fire(){
		animator.SetInteger ("throwKnife", 1);
	}

	void OnShoot() {
		if (projectile) {
			Vector2 pos = transform.position;
			if (lookingRight) {
				pos.x += .32f;
				pos.y += .4f;
			} else {
				pos.x -= .32f;
				pos.y += .4f;
			}
			Projectile clone = Instantiate(projectile, pos, Quaternion.identity) as Projectile;

			Vector2 vect = new Vector2 (-0.4f, 0.4f);
			clone.transform.position = pos;
			clone.transform.localScale = vect;

	    }

	}

	public void die()
	{
		//Debug.Log ("die");
		if (ans < 0) {
			GetComponent<Rigidbody2D>().AddForce (new Vector2 (200f, 0f));
		} else {
			GetComponent<Rigidbody2D>().AddForce (new Vector2 (-200f, 0f));
		}
		animator.SetInteger ("throwKnife", 3);
		Destroy (this.gameObject, .75f);
		
	}

	public void takeDamage()
	{
		MaxHealth -= 20;
	}
}
