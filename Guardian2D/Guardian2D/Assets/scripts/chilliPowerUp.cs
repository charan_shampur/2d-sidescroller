﻿using UnityEngine;
using System.Collections;

public class chilliPowerUp : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D other )
	{
		//Debug.Log ("Chilly Colider");
		if(other.gameObject.tag == "Player"){

			other.gameObject.SendMessage("activateShield");
			Destroy(this.gameObject);

		}
	}
}
