﻿using UnityEngine;
using System.Collections;

public class Lassi : MonoBehaviour {

	aura auraObj;
	void OnCollisionEnter2D(Collision2D other )
	{
		//Debug.Log ("Lassi Colider");
		if(other.gameObject.tag == "Player"){
			
			other.gameObject.SendMessage("destroyShield");
			//PlayerMovement1.fireActive=false;
			auraObj = FindObjectOfType<aura>();
			if(auraObj!=null)
				auraObj.DestroyAura();
			//GameObject auraObj = GameObject.Find("Auras_0");
			//auraObj.SendMessage("DestroyAura");	
			Destroy(this.gameObject);
			
		}
	}
}
