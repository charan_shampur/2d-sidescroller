﻿using UnityEngine;
using System.Collections;

public class DoorClosetrig : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D other )
	{
		//Debug.Log ("door close Colider");
		if(other.gameObject.tag == "Player"){
			
			GameObject obj = GameObject.Find("doorOpen");
			obj.SendMessage("startClose");
			Destroy(this.gameObject);
			
		}
	}
}
