﻿using UnityEngine;
using System.Collections;

public class EnemyBulletForce : MonoBehaviour {

	// Use this for initialization
	public float speed;
	public bool OnlyGoLeft = false;
	public bool OnlyGoRight = false;
	public GameObject explosion;
	Vector2 temp;
	void Start () {
		if(!ShootingEnemyScript.facingRight)
		{
			OnlyGoLeft = true;
			temp=transform.localScale;
			temp.x*=-1;
			transform.localScale = temp;
		}
		if(ShootingEnemyScript.facingRight)
		{
			OnlyGoRight = true;
		}
	
	}
	
	// Updae is called once per frame
	void Update () {
		if (OnlyGoLeft == true)
		{
			GetComponent<Rigidbody2D>().AddForce(Vector3.left * speed);
		}
		
		if (OnlyGoRight == true)
		{
			GetComponent<Rigidbody2D>().AddForce(Vector3.right * speed);
		}
	
	}

	void OnCollisionEnter2D(Collision2D other )
	{
		//Debug.Log ("bullet Colider");

		if(other.gameObject.tag != "turretAmmo")
		{
			if (explosion!=null)
			{
				Instantiate (explosion, transform.position, transform.rotation);
			}

			if(other.gameObject.tag == "Player")
			{
				other.gameObject.SendMessage("takeDamage");
			}

			Destroy(this.gameObject);
		}


	}

	void FixedUpdate()
	{
		//Instantiate (explosion, transform.position, transform.rotation);
		//Destroy (this.explosion);
		Destroy (this.gameObject, 2.5f);

		}

}
