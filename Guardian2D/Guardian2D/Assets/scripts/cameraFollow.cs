﻿using UnityEngine;
using System.Collections;

public class cameraFollow : MonoBehaviour {

	// Use this for initialization
	public GameObject targetObject;

	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		float targetObjectX = targetObject.transform.position.x;
		Vector3 newCameraPosition = transform.position;
		newCameraPosition.x = targetObjectX + 2f;
		transform.position = newCameraPosition;

	}
}
