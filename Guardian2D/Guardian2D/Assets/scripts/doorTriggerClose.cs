﻿using UnityEngine;
using System.Collections;

public class doorTriggerClose : MonoBehaviour {

	public Transform start,end;
	public bool triggered;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		Debug.DrawLine (start.position, end.position, Color.green);
		triggered = Physics2D.Linecast (start.position, end.position);
		Debug.Log (triggered);
		if(triggered)
		{
			GameObject obj = GameObject.Find("doorOpen");
			obj.SendMessage("startClose");
			Destroy(this.gameObject);
		} 
	} 
}
