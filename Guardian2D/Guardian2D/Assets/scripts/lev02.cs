﻿using UnityEngine;
using System.Collections;

public class lev02 : MonoBehaviour {
	public Font font; //set it in the inspector
	private GUIStyle guiStyle;
	
	public void OnGUI()
	{
		const int buttonWidth = 300;
		const int buttonHeight = 150;
		
		guiStyle = new GUIStyle("button");
		guiStyle.font = font;
		//guiStyle.normal.textColor = Color.white;
		guiStyle.fontSize = 100;  //must be int, obviously...
		// Determine the button's place on screen
		// Center in X, 2/3 of the height in Y
		Rect buttonRect = new Rect(
			Screen.width / 2 - (buttonWidth / 2),
			(2 * Screen.height / 3) - (buttonHeight / 2),
			buttonWidth,
			buttonHeight
			);
		
		// Draw a button to start the game
		if(GUI.Button(buttonRect,"Next!", guiStyle))
		{
			// On Click, load the first level.
			// "Stage1" is the name of the first scene we created.
			Application.LoadLevel("Screen3");
		}
	}
}
