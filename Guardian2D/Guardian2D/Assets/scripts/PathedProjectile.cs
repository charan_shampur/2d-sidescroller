using UnityEngine;
using System.Collections;

public class PathedProjectile : MonoBehaviour
{
	private Transform _destination;
	private float _speed;
	public GameObject explosion;

	public void Initialize(Transform destination,float speed)
	{
		_destination = destination;
		_speed = speed;
	}

	public void Update()
	{
		transform.position = Vector3.MoveTowards (transform.position,_destination.position,Time.deltaTime * _speed);
        
		var distanceSquared = (_destination.transform.position - transform.position).sqrMagnitude;
		if(distanceSquared > 0.01f * .01f)
			return;
		Instantiate(explosion,transform.position,transform.rotation);
		Destroy(gameObject);
	}

	void OnCollisionEnter2D(Collision2D other )
	{
		//Debug.Log ("door close Colider");
		if(other.gameObject.tag != "turretAmmo"){
			
			//GameObject obj = GameObject.Find("doorOpen");
			//obj.SendMessage("startClose");
			if(other.gameObject.tag == "Player")
			{
				other.gameObject.SendMessage("takeDamage");
			}
			Instantiate(explosion,transform.position,transform.rotation);
			Destroy(this.gameObject);
			
		}
	}
}