﻿using UnityEngine;
using System.Collections;

public class pointstar : MonoBehaviour, Iplayerrespawnlistener
{
	public GameObject explosion;

	public void OnTriggerEnter2D (Collider2D other)
	{

		if((other.gameObject.tag != "Player")&&(other.gameObject.tag != "Obstacle"))
		{
			gameObject.SetActive (false);
		} 

	}


	public void OnPlayerRespawnInThisCheckpoint( checkpoint ckpt, PlayerMovement1 P1)
	{
		gameObject.SetActive (true);
	}

}
