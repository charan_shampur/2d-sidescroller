﻿using UnityEngine;
using System.Collections;

public class autodestroystar : MonoBehaviour {

	private ParticleSystem _particlesystem;
	public void Start () 
	{
		_particlesystem = GetComponent<ParticleSystem> ();
	}

	public void Update () 
	{
	if (_particlesystem.isPlaying)
						return;
		Destroy (gameObject);
	}
}
