﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class levelmanager : MonoBehaviour 

{
	public static levelmanager Instance { get ; private set; }
	public PlayerMovement1 P1 { get ; private set;}
	public cameraController Cam { get ; private set; }
	public TimeSpan RunningTime { get { return DateTime.UtcNow - _started; } }
    
	public int CurrentTimeBonus
	{
		get
		{
			var secondDiff = (int) (bonuscutoffsecs - RunningTime.TotalSeconds);
			return Mathf.Max (0,secondDiff) * bonussecondmultiplier;
		}
	}

	private List <checkpoint> _checkpoints; 
	private int _currentcheckpointIndex;
	private DateTime _started; // for points
	private int _savedpoints;


	public checkpoint debugspawn;
	public int bonuscutoffsecs;
	public int bonussecondmultiplier;

	public void Awake()
	{
		Instance = this;
	}

	public void Start()
	{
		_checkpoints = FindObjectsOfType<checkpoint> ().OrderBy (t => t.transform.position.x).ToList ();
		_currentcheckpointIndex = _checkpoints.Count > 0 ? 0 : -1;

		P1 = FindObjectOfType<PlayerMovement1> ();
		Cam = FindObjectOfType<cameraController> ();

		_started = DateTime.UtcNow; // indicates start of checkpoint

		// To associate objects in between checkpoints to respawn
		// Based on distance, objects are assigned to checkpoints
		
		var listeners = FindObjectsOfType<MonoBehaviour> ().OfType<Iplayerrespawnlistener> ();
		
		foreach (var listener in listeners) 
		{
			for(var i = _checkpoints.Count - 1; i>= 0; i--)
			{
				var distance = ((MonoBehaviour)listener).transform.position.x - _checkpoints[i].transform.position.x;
				if (distance < 0)
					continue;
				
				_checkpoints[i].assignobjecttockpt(listener); // found first checkpoint within range of object
				break;
			}
		}

#if UNITY_EDITOR
		if (debugspawn != null)
			debugspawn.spawnplayer (P1);
		else if(_currentcheckpointIndex != -1)
			_checkpoints[_currentcheckpointIndex].spawnplayer (P1);
#else
		if(_currentcheckpointIndex != -1)
			_checkpoints[_currentcheckpointIndex].spawnplayer (P1);
#endif
	}
	public void Update()
	{
		var isatlastcheckpt = _currentcheckpointIndex + 1 >= _checkpoints.Count;
		if (isatlastcheckpt)
						return;

		var distancetonxtchkpt = _checkpoints [_currentcheckpointIndex + 1].transform.position.x - P1.transform.position.x;
		if (distancetonxtchkpt >= 0)
						return;
		_checkpoints [_currentcheckpointIndex].playerleftcheckpt ();
		_currentcheckpointIndex++;
		_checkpoints [_currentcheckpointIndex].playerhitcheckpt ();

		gamemanager.Instance.addpoints (CurrentTimeBonus);
		_savedpoints = gamemanager.Instance.Points;
		_started = DateTime.UtcNow;

	}

	public void killplayer()
	{
		StartCoroutine (killplayerco ());
	}

	private IEnumerator killplayerco()
	{
		P1.Kill ();

		Cam.IsFollowing = false;

		yield return new WaitForSeconds (.01f);

		Cam.IsFollowing = true;

		if (_currentcheckpointIndex != -1)
			_checkpoints[_currentcheckpointIndex].spawnplayer (P1);

		_started = DateTime.UtcNow;
		gamemanager.Instance.ResetPts (_savedpoints);
	}
}
