﻿using UnityEngine;
using System.Collections;

public class EnemyMovementOpp : MonoBehaviour {
	public GameObject player;
	bool lookingRight;
	public Animator animator;
	Vector3 r;
	public int MinDist = 5;
	public int MoveSpeed = 40;
	public float MaxDist = 2.0f;
	public float speed = 10f;
	public float forceX = 0f;
	public float forceY = 0f;
	public bool stop = false,collided=false;
	//public int MaxHealth = 100;
	public Vector2 maxVelocity = new Vector2(1,5);
	public float punchDelay = .5f,punchTime;
	public GameObject key;
	private int count;
	public float Maxhealth = 100f;
	public float health{ get; private set; }


	// Use this for initialization
	void Start () {
		lookingRight = true;
		r = transform.right * 1;
		animator = GetComponent<Animator> ();
		animator.SetInteger ("enemyAnim", 0);
		health = 100f;
	}
	
	// Update is called once per frame
	void Update () {

		var absVelX = Mathf.Abs (GetComponent<Rigidbody2D>().velocity.x);
		var absVelY = Mathf.Abs (GetComponent<Rigidbody2D>().velocity.y);
	
		if ((Vector3.Distance (player.transform.position, transform.position) < MaxDist) && !stop) {
			//Debug.Log(MaxDist);
			animator.SetInteger ("enemyAnim", 1);
						//Debug.Log (Vector3.Distance(player.transform.position, transform.position));
						float dist = Vector3.Distance(player.transform.position, transform.position);
						// Get a direction vector from us to the target
						Vector3 dir = player.transform.position - transform.position;
			
						// Normalize it so that it's a unit direction vector
						dir.Normalize ();
//			
						dir.z = 0;
						if (absVelX < maxVelocity.x) {
							forceX = speed;
							GetComponent<Rigidbody2D>().AddForce (new Vector2 (dir.x * speed, 0));
			        	}
//			// Move ourselves in that direction
//			transform.position += dir * MoveSpeed * Time.deltaTime;
				} else {
				GetComponent<Rigidbody2D>().AddForce (new Vector2 (0, 0));
				if(!collided)
				animator.SetInteger ("enemyAnim", 0);
				}
		Vector3 t = player.transform.position - transform.position;
		t.Normalize ();
		float ans = Vector3.Dot (r, t);
		if(ans < 0 && lookingRight) {
			Vector3 newpos = this.transform.position;
			newpos.z -=1;
			lookingRight = false;
			this.transform.LookAt(newpos);
			
		}
		if (ans > 0 && !lookingRight) {
			Vector3 newpos = this.transform.position;
			newpos.z += 1;
			lookingRight = true;
			this.transform.LookAt (newpos);
		}

		if (health < 0) {
			die();
				}
	}

	public void die()
	{
		if(count==1)
			Instantiate (key, transform.position, transform.rotation);
		count++;
		//Debug.Log ("die");
		if (lookingRight) {
			//int i=0;
						//while(i < 10)
			//{
						GetComponent<Rigidbody2D>().AddForce (new Vector2 (-200f, 0f));
			//			i++;
			//}
				} else {
			GetComponent<Rigidbody2D>().AddForce (new Vector2 (200f, 0f));
				}
		animator.SetInteger ("enemyAnim", 2);
		GameObject lightObj = GameObject.Find("enemyHealthbar");
		lightObj.SendMessage("destroy");
		Destroy (this.gameObject, .40f);


		}

	void OnCollisionEnter2D(Collision2D other )
	{
		//Debug.Log ("EnemyPlayer Colider");
		if(other.gameObject.tag == "Player"){
			animator.SetInteger ("enemyAnim", 4);
			other.gameObject.SendMessage("takeDamage");
			stop = true;
			collided=true;
			punchTime = punchDelay;

			//other.gameObject.SendMessage("activateShield");
			//Destroy(this.gameObject);
			
		}
	}

	void OnCollisionExit2D(Collision2D other )
	{
		//Debug.Log ("EnemyPlayer Colider exit");
		if(other.gameObject.tag == "Player"){
			//animator.SetInteger ("enemyAnim", 0);
			stop = false;
			collided=false;
			//other.gameObject.SendMessage("activateShield");
			//Destroy(this.gameObject);
			
		}
	}

	void OnCollisionStay2D(Collision2D other )
	{
		//Debug.Log ("EnemyPlayer Colider exit");
		if(other.gameObject.tag == "Player"){
			//animator.SetInteger ("enemyAnim", 0);
			animator.SetInteger ("enemyAnim", 4);
			punchTime -= Time.deltaTime;
			if(punchTime < 0)
			{	
				animator.SetInteger ("enemyAnim", 0);
				other.gameObject.SendMessage("takeDamage");
				punchTime = punchDelay;
			}


			stop = true;
			collided=true;


			//other.gameObject.SendMessage("activateShield");
			//Destroy(this.gameObject);
			
		}
	}

	public void takeDamage()
	{
		health -= 5f;
		}
}
