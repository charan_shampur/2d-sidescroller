﻿using UnityEngine;
using System.Collections;

public class GameOverScript : MonoBehaviour {
	public Font font; //set it in the inspector
	private GUIStyle guiStyle;
	public void OnGUI()
	{
		const int buttonWidth = 300;
		const int buttonHeight = 150;
		guiStyle = new GUIStyle("button");
		guiStyle.font = font;
		//guiStyle.normal.textColor = Color.white;
		guiStyle.fontSize = 100;  //must be int, obviously...


			GUI.Button (
			// Center in X, 1/3 of the height in Y
			new Rect (
			Screen.width / 2 - (buttonWidth / 2),
			(1 * Screen.height / 3) - (buttonHeight / 2),
			buttonWidth,
			buttonHeight
				),
			"Retry!", guiStyle
				);

			// Reload the level
			//Application.LoadLevel("NewLevel01");

		
		if (
			GUI.Button(
			// Center in X, 2/3 of the height in Y
			new Rect(
			Screen.width / 2 - (buttonWidth / 2),
			(2 * Screen.height / 3) - (buttonHeight / 2),
			buttonWidth,
			buttonHeight
			),
			"Back to menu", guiStyle
			)
			)
		{
			// Reload the level
			Application.LoadLevel("Menu");
		}
	}
}
