﻿using UnityEngine;
using System.Collections;

public class Levelchange : MonoBehaviour {
	
	public void LevelOne()
	{
		Application.LoadLevel ("camera");
	}
	
	public void LevelTwo()
	{
		Application.LoadLevel ("combat");
	}

	public void LevelThree()
	{
		Application.LoadLevel ("parallaxing");
	}
}