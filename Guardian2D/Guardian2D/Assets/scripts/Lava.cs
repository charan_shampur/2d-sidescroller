﻿using UnityEngine;
using System.Collections;

public class Lava : MonoBehaviour {
	public GameObject explosion;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter2D(Collision2D other )
	{
		//Debug.Log ("Platform Colider");
		if(other.gameObject.tag == "Player"){
			other.gameObject.SendMessage("death");
			ContactPoint2D contact = other.contacts[0];
			Quaternion rot = Quaternion.FromToRotation(Vector2.up, contact.normal);
			Vector2 explosionPoint = contact.point;
			Instantiate(explosion,explosionPoint,rot);

		}
		if(other.gameObject.tag == "Obstacle")
		{
			Destroy(other.gameObject);
			//other.gameObject.SetActive (true);
			ContactPoint2D contact = other.contacts[0];
			Quaternion rot = Quaternion.FromToRotation(Vector2.up, contact.normal);
			Vector2 explosionPoint = contact.point;
			Instantiate(explosion,explosionPoint,rot);
		} 
		//other.gameObject.SendMessage("activateShield");
		//Destroy(this.gameObject);
		
	}
	
}
