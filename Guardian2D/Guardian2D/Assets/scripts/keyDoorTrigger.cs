﻿using UnityEngine;
using System.Collections;

public class keyDoorTrigger : MonoBehaviour {

	public int doorIdentifier;
	void OnCollisionEnter2D(Collision2D other )
	{
		//Debug.Log ("doorOpen Colider");
		if(other.gameObject.tag == "Player"){
			
			//other.gameObject.SendMessage("destroyShield");
			//PlayerMovement1.fireActive=false;
			if(doorIdentifier == 1)
			{
				GameObject obj = GameObject.Find("doorOpen");
				obj.SendMessage("startOpen");
				Destroy(this.gameObject);
			}

			if(doorIdentifier ==2)
			{
				GameObject obj = GameObject.Find("doorClose");
				obj.SendMessage("startOpen");
				Destroy(this.gameObject);
			}
			
		}
	}
}
