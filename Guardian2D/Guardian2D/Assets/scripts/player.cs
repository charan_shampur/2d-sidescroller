﻿using UnityEngine;
using System.Collections;

public class player : MonoBehaviour {

	// Use this for initialization
	public float speed = 10f;
	public Vector2 maxVelocity = new Vector2(3,5);
	private PlayerController controller;
	public bool standing;
	public float airSpeedMultiplier = .3f;
	private Animator animator;
	float jumpTime,tauntTime,fireTime, jumpDelay = .5f, tauntDelay =.7f, fireDelay = .7f;
	bool jumped,grounded = false,taunted,fired;
	public Transform groundedEnd;
	public bool interact = false;
	public Transform lineStart, lineEnd;
	//private Animator animator;
	void Start () {
	
		controller = GetComponent<PlayerController> ();
		animator = GetComponent<Animator> ();
		//grounded = true;
	}
	
	// Update is called once per frame
	void Update () {
		rayCasting ();
		movement ();

}

	void rayCasting()
	{

		Debug.DrawLine (this.transform.position, groundedEnd.position, Color.green);
		grounded = Physics2D.Linecast (this.transform.position, groundedEnd.position, 1 << LayerMask.NameToLayer ("ground"));
		//Debug.DrawLine (lineStart.position, lineEnd.position, Color.green);
		//if (Physics2D.Linecast (lineStart.position, lineEnd.position, 1 << LayerMask.NameToLayer ("Guard"))) {

				//}
		//Debug.DrawLine (this.transform.position, groundedEnd.position, Color.green);

		}
	void OnDestroy()
	{
		// Game Over.
		// Add the script to the parent because the current game
		// object is likely going to be destroyed immediately.
		transform.parent.gameObject.AddComponent<GameOverScript>();
	}
	void movement()
	{
		var forceX = 0f;
		var forceY = 0f;
		
		var absVelX = Mathf.Abs (GetComponent<Rigidbody2D>().velocity.x);
		var absVelY = Mathf.Abs (GetComponent<Rigidbody2D>().velocity.y);
		/*
		if (absVelY < .2f) {
			standing = true;
		} else {
			standing = false;
		} */
		
		 //old code start
		if (Input.GetKey ("right")) {
						if (absVelX < maxVelocity.x) {
								forceX = speed;
								transform.localScale = new Vector3 (1, 1, 1);
						}
						animator.SetInteger ("AnimState", 1);
				} else if (Input.GetKey ("left")) {
						if (absVelX < maxVelocity.x) {
								forceX = -speed;
								transform.localScale = new Vector3 (-1, 1, 1);
						}
						animator.SetInteger ("AnimState", 1);
				} else {
			animator.SetInteger ("AnimState", 0);
				}  
		  //old code end 
		/*
		if (controller.moving.x != 0) {
			if (absVelX < maxVelocity.x) {
				
				forceX = standing ? speed * controller.moving.x : (speed * controller.moving.x * airSpeedMultiplier);
				
				transform.localScale = new Vector3 (forceX > 0 ? 1 : -1, 1, 1);
			}
			animator.SetInteger ("AnimState", 1);
		} else {
			animator.SetInteger ("AnimState", 0	);
		}  */
		
		if (Input.GetKeyDown (KeyCode.Space) && grounded)
		{
			GetComponent<Rigidbody2D>().AddForce(transform.up * 200f);
			jumpTime = jumpDelay;
			animator.SetTrigger("jump");
			jumped=true;
		}
		
		jumpTime -= Time.deltaTime;
		if (jumpTime <= 0 && grounded && jumped) {
			animator.SetTrigger("land");
			jumped=false;
				}

		if (Input.GetKeyDown (KeyCode.T) && grounded && !taunted)
		{
			tauntTime = tauntDelay;
			animator.SetTrigger("taunt");
			taunted=true;
		}
		
		tauntTime -= Time.deltaTime;
		if (tauntTime <= 0 && grounded && taunted) {
			animator.SetTrigger("land");
			taunted=false;
		}

		if (Input.GetKeyDown (KeyCode.F) && grounded && !fired)
		{
			fireTime = fireDelay;
			animator.SetTrigger("fire");
			fired=true;
		}
		
		fireTime -= Time.deltaTime;
		if (fireTime <= 0 && grounded && fired) {
			animator.SetTrigger("land");
			fired=false;
		}
		
		GetComponent<Rigidbody2D>().AddForce (new Vector2 (forceX, forceY));

		}
}