﻿using UnityEngine;
using System.Collections;

public class TouchButtonLogic : MonoBehaviour {
	
	
	// Update is called once per frame
	void Update () {
		if(Input.touches.Length <= 0)
		{
			
			//if there are no touches
			
			
		}
		else
		{
			//if there is a touch
			//loop through all the touches on screen
			for(int i=0;i<Input.touchCount;i++)
			{
				//execute this code for current touch on screen
				if(this.GetComponent<GUITexture>().HitTest(Input.GetTouch(i).position))
				{
					//if current touch hits our guitexture, run this code
					if(Input.GetTouch(i).phase == TouchPhase.Began)
					{
						Debug.Log("The touch has begun" + this.name);
					}
					if(Input.GetTouch(i).phase == TouchPhase.Ended)
					{
						Debug.Log("The touch has ended" + this.name);
					}
				}
			}
		}
	}
}