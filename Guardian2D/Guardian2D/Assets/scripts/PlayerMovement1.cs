﻿using UnityEngine;
using System.Collections;

public class PlayerMovement1 : MonoBehaviour {
	
	// Use this for initialization
	public LayerMask detectWhat;
	public LayerMask detectCollideJump;
	public float speed = 7f;
	public Vector2 maxVelocity = new Vector2(3,5);
	private PlayerController controller;
	public bool standing,touched=true;
	public float airSpeedMultiplier = .3f;
	public Animator animator;
	public float jumpTime,tauntTime,fireTime, jumpDelay = .5f, tauntDelay =.7f, fireDelay = .7f;
	public bool jumped=false,grounded = true,taunted,fired=false;
	public Transform groundedEnd;
	public bool interact = false,shielded=false;
	public bool flying;
	public static bool fireActive = false;
	public static bool cinnamonFire = false;
	public Transform lineStart, lineEnd;
	public Transform muzzleFlashPrefab;
	public Transform shield;
	public static bool facingRight = true;
	public RaycastHit2D whatHit;
	public float Maxhealth = 100f;
	public float health{ get; private set; }
	public Vector2 initialPos;
	public float jumpSpeed = 300f,jetSpeed=15f;
	public GameObject auraShield,jetPack;
	JetPack jetP;
	bool hitWall;
	public static int level;
	public Hashtable inGredients = new Hashtable();
	public GameObject GravityLessPlatform1;
	Vector2 g1 = new Vector2(60.82924f,0.02540502f);
	//Vector2 g2 = new Vector2(58.04155f,1.909412f);
	//Vector2 g3 = new Vector2(61.67368f,1.43539f);
	//Vector2 g4 = new Vector2(65.01564f,0.9403892f);
	aura auraObj;
	//public bool jumpTouch = false;


	//private Animator animator;
	void Start () {
		
		controller = GetComponent<PlayerController> ();
		animator = GetComponent<Animator> ();
		animator.SetInteger ("AnimState", 0);
		health = 100f;
		initialPos.x = transform.position.x;
		initialPos.y = transform.position.y;
		inGredients.Add("Soda","false");
		inGredients.Add ("CorainderPowder", "false");
		inGredients.Add ("CuminPowder", "false");
		inGredients.Add ("DenSalt", "false");
		inGredients.Add ("DymangoPowder", "false");
		inGredients.Add ("GaramMasala", "false");
		inGredients.Add ("Ghee", "false");
		inGredients.Add ("Ginger", "false");
		inGredients.Add ("Semolina", "false");
		inGredients.Add ("ToorDal", "false");
		inGredients.Add ("Turmeric", "false");
		inGredients.Add ("WheatFlour", "false");
		//Instantiate(GravityLessPlatform1,g1,Quaternion.identity);
		//Instantiate(GravityLessPlatform2,g2,Quaternion.identity);
		//Instantiate(GravityLessPlatform3,g3,Quaternion.identity);
		//Instantiate(GravityLessPlatform4,g4,Quaternion.identity);

		//Vector3 theScale = transform.localScale;
		//theScale.x *= -1;
		//transform.localScale = theScale;

		//grounded = true;
	}
	
	// Update is called once per frame
	void Update () {
		//debug.log ("Touch count =");
		//debug.log (Input.touchCount);
		/*
		if (flying && jumpTouch)
		{
						rigidbody2D.gravityScale = 0f;
						rigidbody2D.mass = 0.1f;
		}
		else
		{
						rigidbody2D.gravityScale = 1f;
						rigidbody2D.mass = 1f;
		}
		*/
		var absVelY = Mathf.Abs (GetComponent<Rigidbody2D>().velocity.y);
		//Debug.Log (absVelY);
		rayCasting ();

		//movement ();
		//animator.SetTrigger ("land");
		//animator.SetInteger ("AnimState", 0);
		jumpTime -= Time.deltaTime;
		if (jumpTime <= 0 && grounded && jumped && !fired) {
			//debug.log("landed");
						animator.SetTrigger ("land");
						jumped = false;
						jetP = FindObjectOfType<JetPack>();
						if(flying)
							jetP.deactivateJetPack();
				}
		if (touched == false) {
			//debug.log("touched = false");
			animator.SetInteger ("AnimState", 0);
			touched=true;
				}
		fireTime -= Time.deltaTime;
		if (fireTime <= 0 && grounded && fired && !jumped) {
			animator.SetTrigger("land");
			fired=false;
		}

		/*
		if (shielded) {
			health-=0.05f;
			Debug.Log("Shield active");
			Transform shieldClone = Instantiate (shield) as Transform;
			Vector2 positMove = transform.position;
			if (facingRight) {
				positMove.x += .35f;
				positMove.y += .4f;
			} else {
				positMove.x -= .35f;
				positMove.y += .4f;
			}
			shieldClone.position=positMove;
			float sizeX = Random.Range (0.2f, 0.7f);
			float sizeY = Random.Range (1.2f, 1.6f);
 			if (facingRight) {
				shieldClone.localScale = new Vector2 (sizeX, sizeY);
			} else {
				shieldClone.localScale = new Vector2 (-sizeX, sizeY);
			}
			Destroy (shieldClone.gameObject, .15f);
		} 
		*/

		if(shielded)
		{
			health-=0.02f;
		}

		if (transform.position.y < -3) {

			//GameOverScript obj = new GameOverScript();
			//obj.OnGUI();

			//transform.position = initialPos;

			health = -1f;
			//levelmanager.Instance.killplayer ();
				}

		if (health < 0f) {
			/*
			Destroy(this.gameObject);
			HealthBar bar;
			bar = FindObjectOfType<HealthBar>();
			bar.destroy();
			*/
			//transform.position = initialPos;
			health = 100f;
			destroyShield();
			//DestroyPlatform.PlayerDied = true;
			//StartCoroutine(WaitASec(1.0F));
			//GameObject fallingObj1 = GameObject.Find("gravityLessPlatform1");
			//if (fallingObj1 == null)
			DestroyPlatform plat;
			plat = FindObjectOfType<DestroyPlatform>();
			plat.destroyPlat();
			Instantiate(GravityLessPlatform1,g1,Quaternion.identity);
			//fallingObj1.SendMessage("ResetPlatform");
			//GameObject fallingObj2 = GameObject.Find("gravityLessPlatform2");
			//if (fallingObj2 == null)
			//Instantiate(GravityLessPlatform2,g2,Quaternion.identity);
			//fallingObj2.SendMessage("ResetPlatform");
			//GameObject fallingObj3 = GameObject.Find("gravityLessPlatform3");
			//if (fallingObj3 == null)
			//Instantiate(GravityLessPlatform3,g3,Quaternion.identity);
			//fallingObj3.SendMessage("ResetPlatform");
			//GameObject fallingObj4 = GameObject.Find("gravityLessPlatform4");
			//if (fallingObj4 == null)
			//Instantiate(GravityLessPlatform4,g4,Quaternion.identity);
			//fallingObj4.SendMessage("ResetPlatform"); 

			DestroyPlatform.PlayerDied = false;
			auraObj = FindObjectOfType<aura>();
			if(auraObj!=null)
				auraObj.DestroyAura();
			levelmanager.Instance.killplayer ();
			//GameOverScript obj = new GameOverScript();
			//obj.OnGUI();

		
				}			
		
		}
		
	IEnumerator WaitASec(float waitTime){
		yield return new WaitForSeconds(waitTime);
		Debug.Log ("Waited a sec");
	}
	public void takeDamage()
	{
		if(!shielded)
			health -= 5f;

	}
	public void death() {

		health = -1f;
    } 
	void rayCasting()
	{
		
		Debug.DrawLine (this.transform.position, groundedEnd.position, Color.green);
		//grounded = Physics2D.Linecast (this.transform.position, groundedEnd.position, 1 << LayerMask.NameToLayer ("ground"));
		grounded = Physics2D.Linecast (this.transform.position, groundedEnd.position, detectWhat);
		hitWall=Physics2D.Linecast (lineStart.position, lineEnd.position, detectCollideJump);
		/*if (grounded) {
			debug.log("Its grounded");
						animator.SetTrigger ("land");
				}
		else debug.log("Its NOT!!! grounded"); */
		Debug.DrawLine (lineStart.position, lineEnd.position, Color.red);
		if (Physics2D.Linecast (lineStart.position, lineEnd.position, 1 << LayerMask.NameToLayer ("Guard"))) {
						interact = true;
			whatHit=Physics2D.Linecast (lineStart.position, lineEnd.position, 1 << LayerMask.NameToLayer ("Guard"));
				} else {
			interact = false;
				}

		//Debug.DrawLine (this.transform.position, groundedEnd.position, Color.green);
		
	}
	public void move(int moveDir)
	{
		followpath.playerOnPlatform = false;
		var forceX = 0f;
		var forceY = 0f;
		var absVelX = Mathf.Abs (GetComponent<Rigidbody2D>().velocity.x);
		var absVelY = Mathf.Abs (GetComponent<Rigidbody2D>().velocity.y);
		//debug.log ("absVelX=");
		//debug.log (absVelX);
		//debug.log ("moveDir=");
		//debug.log (moveDir);
		//debug.log("inside move");
		if (moveDir == 1) 
		{
				if (absVelX < maxVelocity.x) 
				{
					//debug.log("adding speed right");
					forceX = speed;
					//forceX = grounded? speed : (speed * airSpeedMultiplier);
					transform.localScale = new Vector3 (1, 1, 1);
				    
				}
				//debug.log("running right");
				animator.SetInteger ("AnimState", 1);
				facingRight = true;
		}
		else
		{
				//animator.SetInteger ("AnimState", 1);
				if (absVelX < maxVelocity.x) 
				{
						//debug.log("adding speed left");
						forceX = -speed;
						//forceX = grounded? -speed : (-speed * airSpeedMultiplier);
						transform.localScale = new Vector3 (-1, 1, 1);
				}
				//debug.log("running left");
				animator.SetInteger ("AnimState", 1);
				facingRight = false;
		}

		GetComponent<Rigidbody2D>().AddForce (new Vector2 (forceX, forceY));
		//rigidbody2D.AddForce (transform.right * forceX);
		//animator.SetInteger ("AnimState", 1);
	}

	public void jump()
	{
		var forceY = 0f;
		var forceX = 0f;
		followpath.playerOnPlatform = false;
		if(!flying)
		{			if (grounded) {
						if (hitWall)
							jumpSpeed = 340f;
						else
							jumpSpeed = 300f;
						GetComponent<Rigidbody2D>().AddForce (transform.up * jumpSpeed);
						jumpTime = jumpDelay;
						animator.SetTrigger ("jump");
						//debug.log("Jumped");
						jumped = true;
					}				
		}
		else
		{
			var absVelY = Mathf.Abs (GetComponent<Rigidbody2D>().velocity.y);
			if(absVelY < maxVelocity.y-0.5f)
				forceY = jetSpeed;
			else
			{
				if(transform.position.y < -0.0f)
					forceY =250f;
				else if(transform.position.y < -0.2f)
				        forceY = 450f;
				else if(transform.position.y < -0.4f)
				        forceY = 550f;
				else if(transform.position.y < -1.2f)
					forceY = 700f;
				else if(transform.position.y < -1.4f)
					forceY = 900f;
			}
			//if(grounded && jumped==false)
			if(jumped == false)
			{
					jumpTime = jumpDelay;
				animator.SetTrigger ("jump");
				Instantiate(jetPack,transform.position,transform.rotation);
			}
				jumped = true;
			//transform.up = transform.up+forceY;
			GetComponent<Rigidbody2D>().AddForce(transform.up*forceY);
			//rigidbody2D.mass=0;
				//rigidbody2D.AddForce (new Vector2 (forceX, forceY));

		}
	}

	public void punch()
	{
		//EnemyMovementOpp obj;
		//obj = FindObjectOfType<EnemyMovementOpp>();
		cinnamonFire = true;
		if (grounded && (fired==false)) {
			fireTime = fireDelay;
			animator.SetTrigger("fire");
			fired=true;
			effect();
			if (interact == true)
			{
				Debug.Log("punch pressed and interact = true");
				GameObject obj = whatHit.collider.gameObject;
				obj.SendMessage("takeDamage");
			}

				}

		}

	public void effect()
	{

		Transform clone = Instantiate (muzzleFlashPrefab) as Transform;
		//BoxCollider clone = Instantiate (muzzleFlashPrefab) as BoxCollider2D;
		//clone.isTrigger = true;
		//clone.localScale = transform.localScale;
		Vector2 pos = transform.position;

		Debug.Log ("facingRight");
		Debug.Log (facingRight);
		if (facingRight) {
						pos.x += .32f;
						pos.y += .4f;
				} else {
			pos.x-=.32f;
			pos.y+=.4f;
				}
		//pos.x += .2f;
		//pos.y += .3f;
		//clone.position = pos;
//		clone.parent = PlayerMovement1;
		clone.position = pos;
		float size = Random.Range (0.6f, 0.8f);
		if (facingRight) {
						clone.localScale = new Vector2 (size, size);
				} else {
			clone.localScale = new Vector2 (-size, size);
				}
		Destroy (clone.gameObject, .35f);
		} 

	public void activateShield()
	{
		/*
				Vector2 posit = transform.position;
				if (facingRight) {
						posit.x += .32f;
						posit.y += .4f;
				} else {
						posit.x -= .32f;
						posit.y += .4f;
				}

				Instantiate (shield, posit, Quaternion.identity); */
				shielded = true;
				Instantiate (auraShield, transform.position, transform.rotation);

		/*
		Transform shieldClone = Instantiate (shield) as Transform;
		Vector2 pos = transform.position;
		if (facingRight) {
			pos.x += .32f;
			pos.y += .4f;
		} else {
			pos.x-=.32f;
			pos.y+=.4f;
		}
		shieldClone.position = pos;
		shield = shieldClone; */

	}

	public void destroyShield()
	{

		shielded = false;
		health = 100;
		//Destroy (auraShield);

		}


	/*public void effect()
	{
		
				Rigidbody clone = Instantiate (muzzleFlashPrefab) as Rigidbody;
				//BoxCollider clone = Instantiate (muzzleFlashPrefab) as BoxCollider2D;
				//clone.isTrigger = true;
				//clone.localScale = transform.localScale;
				Vector3 pos = transform.position;
		
				debug.log ("facingRight");
				debug.log (facingRight);
				if (facingRight) {
						pos.x += .2f;
						pos.y += .3f;
				} else {
						pos.x -= .2f;
						pos.y += .3f;
				}
				//pos.x += .2f;
				//pos.y += .3f;
				//clone.position = pos;
				//		clone.parent = PlayerMovement1;
				clone.position = pos;
				float size = Random.Range (0.6f, 0.9f);
				if (facingRight) {
						clone.position = new Vector3 (size, size, size);
				} else {
						clone.position = new Vector3 (-size, size, size);
				}
				Destroy (clone.gameObject, 100f);
		}

*/

	public void Kill()
	{
		return;
	
	}
	public void respawn(Transform spawnpt)
	{

		transform.position = spawnpt.position;
	}
	/*
	void movement()
	{

		var forceX = 0f;
		var forceY = 0f;
		
		var absVelX = Mathf.Abs (rigidbody2D.velocity.x);
		var absVelY = Mathf.Abs (rigidbody2D.velocity.y);
		 

		if (Input.GetKey ("right")) {

			if (absVelX < maxVelocity.x) {
				forceX = speed;
				transform.localScale = new Vector3 (1, 1, 1);
			}
			animator.SetInteger ("AnimState", 1);
		} else if (Input.GetKey ("left")) {
		
			if (absVelX < maxVelocity.x) {
				forceX = -speed;
				transform.localScale = new Vector3 (-1, 1, 1);
			}
			animator.SetInteger ("AnimState", 1);
		} else {
			if (!touched)
			{
			debug.log("idle");
			animator.SetInteger ("AnimState", 0);
			}
		} 


		if (Input.GetKeyDown (KeyCode.Space) && grounded)
		{
			GetComponent<Rigidbody2D>().AddForce(transform.up * 200f);
			jumpTime = jumpDelay;
			animator.SetTrigger("jump");
			jumped=true;
		}
		
		jumpTime -= Time.deltaTime;
		if (jumpTime <= 0 && grounded && jumped) {
			animator.SetTrigger("land");
			jumped=false;
		}
		
		if (Input.GetKeyDown (KeyCode.T) && grounded && !taunted)
		{
			tauntTime = tauntDelay;
			animator.SetTrigger("taunt");
			taunted=true;
		}
		
		tauntTime -= Time.deltaTime;
		if (tauntTime <= 0 && grounded && taunted) {
			animator.SetTrigger("land");
			taunted=false;
		}
		
		if (Input.GetKeyDown (KeyCode.F) && grounded && !fired)
		{
			fireTime = fireDelay;
			animator.SetTrigger("fire");
			fired=true;
		}
		
		fireTime -= Time.deltaTime;
		if (fireTime <= 0 && grounded && fired) {
			animator.SetTrigger("land");
			fired=false;
		}
		
		//rigidbody2D.AddForce (new Vector2 (forceX, forceY));
		
	} */
}