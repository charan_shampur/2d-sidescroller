﻿using UnityEngine;
using System.Collections;

public class EdgeCollision : MonoBehaviour {
	
	public PlayerMovement1 player;
	public bool flag = true;
	public Transform dal_bati;
	public Transform chicken;
	public Transform dosa;

	
	void Start () {
		player = FindObjectOfType<PlayerMovement1>();
	}
	
	
	void OnCollisionEnter2D(Collision2D other )
	{
		Debug.Log ("Edge Colider");
		if(other.gameObject.tag == "Player"){
			
			//PlayerMovement1.fireActive=true;
			//Destroy(this.gameObject);
			
			foreach (DictionaryEntry entry in player.inGredients)
			{
				Debug.Log(entry);
				if(entry.Value == "true") {
					continue;
					
				} else {
					flag = false;
					break;
				}
			}
			if(flag) {
				
				Vector2 pos = new Vector2(player.transform.position.x, player.transform.position.y);
				pos.x-=5f;
				pos.y+=2.5f;
				if(PlayerMovement1.level == 3) {
					Instantiate(dal_bati, pos, Quaternion.identity); 
					MenuScript gameOver;
					gameOver = gameObject.AddComponent<MenuScript>() as MenuScript;
					gameOver.OnGUI();
				} else if (PlayerMovement1.level == 1) {
					Instantiate(chicken, pos, Quaternion.identity); 
					MenuScript1 menuscript1;
					menuscript1 = gameObject.AddComponent<MenuScript1>() as MenuScript1;
					menuscript1.OnGUI();
				} else if (PlayerMovement1.level == 2) {
					Instantiate(dosa, pos, Quaternion.identity); 
					MenuScript2 menuscript2;
					menuscript2 = gameObject.AddComponent<MenuScript2>() as MenuScript2;
					menuscript2.OnGUI();
					
				}
				
				
			} else {
				if(PlayerMovement1.level == 3) {
					//Instantiate(dal_bati, pos, Quaternion.identity); 
					MenuScript gameOver;
					gameOver = gameObject.AddComponent<MenuScript>() as MenuScript;
					gameOver.OnGUI();
				} else if (PlayerMovement1.level == 1) {
					//Instantiate(dal_bati, pos, Quaternion.identity); 
					MenuScript1 menuscript1;
					menuscript1 = gameObject.AddComponent<MenuScript1>() as MenuScript1;
					menuscript1.OnGUI();
				} else if (PlayerMovement1.level == 2) {
					//Instantiate(dal_bati, pos, Quaternion.identity); 
					MenuScript2 menuscript2;
					menuscript2 = gameObject.AddComponent<MenuScript2>() as MenuScript2;
					menuscript2.OnGUI();
					
				}
			}
			
		}
	}
}
