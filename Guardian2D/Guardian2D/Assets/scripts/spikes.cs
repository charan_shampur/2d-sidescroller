﻿using UnityEngine;
using System.Collections;

public class spikes : MonoBehaviour {

	public GameObject explosion;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D other )
	{
		//Debug.Log ("Platform Colider");
		if(other.gameObject.tag == "Player"){
				other.gameObject.SendMessage("takeDamage");
				ContactPoint2D contact = other.contacts[0];
				Quaternion rot = Quaternion.FromToRotation(Vector2.up, contact.normal);
				Vector2 explosionPoint = contact.point;
				Instantiate(explosion,explosionPoint,rot);
				Vector2 vec = other.gameObject.transform.position;
				vec.x = vec.x -	 .8f;
				other.gameObject.transform.position = vec;
			}
			//other.gameObject.SendMessage("activateShield");
			//Destroy(this.gameObject);
			
		}

	void OnCollisionStay2D(Collision2D other) {

		if(other.gameObject.tag == "Player"){
			other.gameObject.SendMessage("takeDamage");
			ContactPoint2D contact = other.contacts[0];
			Quaternion rot = Quaternion.FromToRotation(Vector2.up, contact.normal);
			Vector2 explosionPoint = contact.point;
			Instantiate(explosion,explosionPoint,rot);
			Vector2 vec = other.gameObject.transform.position;
			vec.x = vec.x -	 .8f;
			other.gameObject.transform.position = vec;
		}

		}
}

