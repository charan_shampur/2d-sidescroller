﻿using UnityEngine;
using System.Collections;

public class ShootingEnemyUnitB : MonoBehaviour {
	
	public float velocity = 1f;
	bool waitActive,canSwitch;
	public Transform sightStart;
	public Transform sightEnd;
	
	public Transform LineOfFireStart;
	public Transform LineOfFireEnd;
	
	public LayerMask detectWhat;
	
	//public Transform weakness;
	
	public bool colliding,firing;
	//public RaycastHit2D fireCollider;
	public int maxhealth = 100;
	public GameObject explosion1,explosion2,enemyBullet;
	public static bool facingRight=true;
	private Vector2 positionLeft,positionRight;
	public Transform muzzleFlashPrefab;
	
	//Animator anim;
	
	
	
	
	// Use this for initialization
	void Start () {
		//anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		positionLeft = new Vector2 (transform.position.x-0.7f,transform.position.y+.25f);
		positionRight = new Vector2 (transform.position.x+0.7f,transform.position.y+.25f);
		
		if(!firing)
			GetComponent<Rigidbody2D>().velocity = new Vector2 (velocity, GetComponent<Rigidbody2D>().velocity.y);
		else
			GetComponent<Rigidbody2D>().velocity = new Vector2 (0f, GetComponent<Rigidbody2D>().velocity.y);
		
		colliding = Physics2D.Linecast (sightStart.position, sightEnd.position, detectWhat);
		firing = Physics2D.Linecast (LineOfFireStart.position, LineOfFireEnd.position,1 << LayerMask.NameToLayer ("Player"));
		
		if (transform.localScale.x > 0)
			facingRight = true;
		else
			facingRight = false;
		
		/*
		if(fireCollider.collider.tag == "Player")
		{
			firing = true;
		}
		else
			firing = false;
		*/
		if (colliding && !firing) {
			
			
			transform.localScale = new Vector2 (transform.localScale.x * -1, transform.localScale.y);
			velocity*= -1;
			
			
		}
		
		if (firing) 
		{
			//yield return new WaitForSeconds(0.8f);
			//Thread.Sleep(1000);
			if(!waitActive){
				StartCoroutine(Wait());   
			}
			if(canSwitch)
			{
				if(facingRight)
					shootRight();
				else
					shootLeft();
				canSwitch = false;
			}
			
		}
		
		
		
		if(maxhealth < 0)
		{
			Instantiate(explosion1,transform.position,transform.rotation);
			Destroy(gameObject);
		}
		
		
		
	}
	IEnumerator Wait(){
		waitActive = true;
		yield return new WaitForSeconds (0.7f);
		canSwitch = true;
		waitActive = false;
	}
	
	void shootLeft()
	{
		//positionLeft.Scale.x = * -1;
		effect ("left");
		Instantiate(enemyBullet,positionLeft,Quaternion.identity);
		
	}
	
	void shootRight()
	{
		effect ("right");
		Instantiate(enemyBullet, positionRight, Quaternion.identity);
	}
	
	void OnDrawGizmos()
	{
		Gizmos.color = Color.magenta;
		
		Gizmos.DrawLine (sightStart.position, sightEnd.position);
		
		Gizmos.color = Color.red;
		
		Gizmos.DrawLine (LineOfFireStart.position, LineOfFireEnd.position);
		
		
	}
	
	void effect(string s)
	{
		Transform clone;
		if (s == "left") {
			positionLeft.x -= 1.0f;
			clone = Instantiate (muzzleFlashPrefab, positionLeft, Quaternion.identity) as Transform;
		}
		else {
			positionRight.x += 1.0f;
			clone = Instantiate(muzzleFlashPrefab, positionRight, Quaternion.identity) as Transform;
		}
		if (facingRight)
		{
			Vector2 pos = clone.localScale;
			pos.x*=-1;
			clone.localScale = pos;
		}
		Destroy (clone.gameObject, .13f);
		
	}
	
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Player") {
			
			other.gameObject.SendMessage ("takeDamage");
			ContactPoint2D contact = other.contacts [0];
			Quaternion rot = Quaternion.FromToRotation (Vector2.up, contact.normal);
			Vector2 explosionPoint = contact.point;
			Instantiate (explosion2, explosionPoint, rot);
			Vector2 vec = other.gameObject.transform.position;
			vec.x = vec.x - .8f;
			other.gameObject.transform.position = vec;
			
		}
		/*
				if (other.gameObject.tag == "turretAmmo")
				{
					transform.localScale = new Vector2 (transform.localScale.x * -1, transform.localScale.y);
					velocity*= -1;
				}
*/
	}
	
	void OnCollisionStay2D(Collision2D other)
	{
		if (other.gameObject.tag == "Player") {
			
			other.gameObject.SendMessage ("takeDamage");
			ContactPoint2D contact = other.contacts [0];
			Quaternion rot = Quaternion.FromToRotation (Vector2.up, contact.normal);
			Vector2 explosionPoint = contact.point;
			Instantiate (explosion2, explosionPoint, rot);
			Vector2 vec = other.gameObject.transform.position;
			vec.x = vec.x - .8f;
			other.gameObject.transform.position = vec;
			
		}
		/*
				if (other.gameObject.tag == "turretAmmo")
				{
					transform.localScale = new Vector2 (transform.localScale.x * -1, transform.localScale.y);
					velocity*= -1;
				}
*/
	}
	
	public void takeDamage()
	{
		maxhealth -= 20;
	}
	
}