﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {
	public GameObject player;
	bool lookingRight;
	Vector3 r;
	// Use this for initialization
	void Start () {
		lookingRight = false;
		r = transform.right * -1;
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 t = player.transform.position - transform.position;

		t.Normalize ();
		float ans = Vector3.Dot (r, t);
		if(ans < 0 && lookingRight) {
			Vector3 newpos = this.transform.position;
			newpos.z -=1;
			lookingRight = false;
			this.transform.LookAt(newpos);

	}
		if (ans > 0 && !lookingRight) {
						Vector3 newpos = this.transform.position;
						newpos.z += 1;
						lookingRight = true;
						this.transform.LookAt (newpos);
				}
}
}