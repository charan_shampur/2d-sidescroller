﻿using UnityEngine;
using System.Collections;

public class JetPack : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		GameObject player = GameObject.Find("Player");
		Vector2 vec = new Vector2(player.transform.position.x,player.transform.position.y);
		if(PlayerMovement1.facingRight){
			transform.localScale = new Vector3 (1, 1, 1);
			vec.x += .1f;
		}
		else{
			transform.localScale = new Vector3 (-1, 1, 1);
			vec.x -= .1f;
		}
		transform.position=vec;
	
	}

	public void deactivateJetPack()
	{
		Destroy (this.gameObject);
	}
}
