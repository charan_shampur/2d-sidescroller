﻿using UnityEngine;
using System.Collections;

public class guipopup : MonoBehaviour {
	public PlayerMovement1 player;
	public ProgressBar bar;
	public Font font; //set it in the inspector
	private GUIStyle guiStyle;

	 Rect windowRect0 = new Rect(300,300,600,300);
	Rect windowRect1 = new Rect(300,300,600,300);
	Rect windowRect2 = new Rect(300,300,600,300);
	Rect windowRect3 = new Rect(300,300,600,300);
	Rect windowRect4 = new Rect(300,300,600,300);
	Rect windowRect5 = new Rect(300,300,600,300);
	Rect windowRect6 = new Rect(300,300,600,300);
	Rect windowRect7 = new Rect(300,300,600,300);
	Rect windowRect8 = new Rect(300,300,600,300);
	Rect windowRect9 = new Rect(300,300,600,300);
	Rect windowRect10 = new Rect(300,300,600,300);
	Rect windowRect11 = new Rect(300,300,600,300);
	Rect windowRect12 = new Rect(300,300,600,300);
	public bool showwindow=false;
	public string popupname;	
	public Texture2D popuplogo;
	public Texture2D background;
	public string popupdesc;
	public GUIStyle s1;
	public GUIStyle pop;
	void Start () {
		player = FindObjectOfType<PlayerMovement1>();
		bar = FindObjectOfType<ProgressBar>();


	}

	// Update is called once per frame
	void Update () {
	
	}
	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Player") {
						showwindow = true;
			//Time.timeScale=0;
			if(player.inGredients.ContainsKey(popupname)) {
				player.inGredients.Remove(popupname);
				player.inGredients.Add(popupname, "true");
			}

		}
	}
	void OnCollisionExit2D(Collision2D coll) {
		if (coll.gameObject.tag == "Player") {
			showwindow = false;
			bar.fill();
			Destroy(this.gameObject);

		}
	}
	void OnGUI() {
		guiStyle = new GUIStyle("button");
		guiStyle.font = font;
		guiStyle.fontSize = 40; 

				if (showwindow) {
						GUI.color = Color.red;
						GUI.color = Color.white;
						GUI.backgroundColor = Color.blue;
						GUI.contentColor = Color.green;
						switch (popupname) {
						case "Soda":
								windowRect0 = GUI.Window (0, windowRect0, DoMyWindow, popupname,s1);
								break;
						case "CorainderPowder":
								windowRect1 = GUI.Window (1, windowRect1, DoMyWindow, popupname,s1);
								break;
						case "CuminPowder":
								windowRect2 = GUI.Window (2, windowRect2, DoMyWindow, popupname,s1);
								break;
						case "DenSalt":
								windowRect3 = GUI.Window (3, windowRect3, DoMyWindow, popupname,s1);
								break;
						case "DymangoPowder":
								windowRect5 = GUI.Window (5, windowRect5, DoMyWindow, popupname,s1);
								break;
						case "GaramMasala":
								windowRect6 = GUI.Window (6, windowRect6, DoMyWindow, popupname,s1);
								break;
						case "Ghee":
								windowRect7 = GUI.Window (7, windowRect7, DoMyWindow, popupname,s1);
								break;
						case "Ginger":
								windowRect8 = GUI.Window (8, windowRect8, DoMyWindow, popupname,s1);
								break;
						case "Semolina":
								windowRect9 = GUI.Window (9, windowRect9, DoMyWindow, popupname,s1);
								break;
						case "ToorDal":
								windowRect10 = GUI.Window (10, windowRect10, DoMyWindow, popupname,s1);
								break;
						case "WheatFlour":
								windowRect11 = GUI.Window (11, windowRect10, DoMyWindow, popupname,s1);
								break;
						case "Turmeric":
								windowRect12 = GUI.Window (12, windowRect10, DoMyWindow, popupname,s1);
								break;
						}
				}
	}
	/*
	void DoMyWindow(int windowID) {
		GUI.Label(new Rect ( 20, 20,180,105),new GUIContent(popuplogo));
			GUI.Label(new Rect (20,125, 380, 105),popupdesc);
		if (GUI.Button (new Rect (220, 20, 80, 40 ), "RESUME")) {
						Time.timeScale = 1.0f;
			showwindow = false;
		}
		GUI.DragWindow(new Rect(0, 0, 10000, 10000));
	} */

	void DoMyWindow(int windowID) {
		GUI.Label(new Rect ( 40, 40,360,200),new GUIContent(popuplogo));
		GUI.Label(new Rect (40,240, 440, 60),popupdesc,pop);
		if (GUI.Button (new Rect (380, 80, 200, 60 ), "RESUME", pop)) {
			Time.timeScale = 1.0f;

			bar.fill();
			Destroy(this.gameObject);
			//	gameObject.SetActive (false);
			showwindow = false;
		}
		//GUI.DragWindow(new Rect(10, 10, 10000, 10000));
	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == "Player") {
			showwindow = false;
		}
	}
	/*
	public void OnPlayerRespawnInThisCheckpoint( checkpoint ckpt, PlayerMovement1 P1)
	{
		
		gameObject.SetActive (true);
		
	} */
}
