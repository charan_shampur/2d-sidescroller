﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class followpath : MonoBehaviour {
	
	public enum FollowType
	{
		MoveTowards,
		Lerp
	}
	
	public FollowType Type = FollowType.MoveTowards;
	public path Path;
	public float Speed = 1;
	public float MaxDistanceToGoal = .1f;
	public static bool playerOnPlatform = false;
	public bool standAllowed;
	public GameObject explosion;
	public bool currentPlatform=false;

	
	private IEnumerator<Transform> _currentPoint;
	
	public void Start()
	{
		if (Path == null)
		{
			Debug.LogError("Path cannot be null", gameObject);
			return;
		}
		
		_currentPoint = Path.GetPathEnumerator();
		_currentPoint.MoveNext();
		
		if (_currentPoint.Current == null)
			return;
		
		transform.position = _currentPoint.Current.position;
	}
	
	public void Update()
	{
		if (_currentPoint == null || _currentPoint.Current == null)
			return;
		if (Type == FollowType.MoveTowards)
			transform.position = Vector3.MoveTowards (transform.position, _currentPoint.Current.position, Time.deltaTime * Speed);
		else if (Type == FollowType.Lerp)
			transform.position = Vector3.Lerp(transform.position, _currentPoint.Current.position, Time.deltaTime * Speed);
		
		var distanceSquared = (transform.position - _currentPoint.Current.position).sqrMagnitude;
		if (distanceSquared < MaxDistanceToGoal * MaxDistanceToGoal)
			_currentPoint.MoveNext();
		if (playerOnPlatform && standAllowed && currentPlatform) {
			PlayerMovement1 player = FindObjectOfType<PlayerMovement1>();
			Vector2 vec = new Vector2(transform.position.x,transform.position.y);
			//Vector2 playerVector = new Vector2(player.transform.position.x,player.transform.position.y);
			Vector2 newPosition = new Vector2();
			//newPosition.x=vec.x+(playerVector.x - vec.x);
			//newPosition.y=vec.y + .7f;
			//player.transform.position = newPosition;
			//player.transform.position.x=vec.x+(playerVector.x - vec.x);
			//player.transform.position.y = vec.y + .7f;
			//vec.x = transform.position.x;
			//vec.y = transform.position.y;
			vec.y = vec.y + .7f;
			newPosition.x = vec.x;
			newPosition.y = vec.y;
			player.transform.position = newPosition;
				}

	}

	void OnCollisionEnter2D(Collision2D other )
	{
		//Debug.Log ("Platform Colider");
		if(other.gameObject.tag == "Player"){
			if(standAllowed)
			{
				playerOnPlatform = true;
				currentPlatform=true;
			}else
			{
				other.gameObject.SendMessage("takeDamage");
				ContactPoint2D contact = other.contacts[0];
				Quaternion rot = Quaternion.FromToRotation(Vector2.up, contact.normal);
				Vector2 explosionPoint = contact.point;
				Instantiate(explosion,explosionPoint,rot);
				Vector2 vec = other.gameObject.transform.position;
				vec.x = vec.x - .8f;
				other.gameObject.transform.position = vec;
			}
			//other.gameObject.SendMessage("activateShield");
			//Destroy(this.gameObject);
			
		}
	}

	void OnCollisionExit2D(Collision2D other )
	{
		//Debug.Log ("Platform Colider");
		if(other.gameObject.tag == "Player"){
			playerOnPlatform = false;
			currentPlatform=false;
			//other.gameObject.SendMessage("activateShield");
			//Destroy(this.gameObject);
			
		}
	}
}

