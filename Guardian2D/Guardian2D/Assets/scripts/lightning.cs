﻿using UnityEngine;
using System.Collections;

public class lightning : MonoBehaviour {

	// Use this for initialization
	public GameObject explosion1;
	//public GameObject explosion2;
	//public int maxhealth = 100;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	
	}

	public void OnCollisionEnter2D(Collision2D other )
	{
		//Debug.Log ("Platform Colider");
		if(other.gameObject.tag == "Player"){
			ContactPoint2D contact = other.contacts[0];
			Quaternion rot = Quaternion.FromToRotation(Vector2.up, contact.normal);
			Vector2 explosionPoint = contact.point;
			Instantiate(explosion1,explosionPoint,rot);
			Vector2 vec = other.gameObject.transform.position;
			vec.x = vec.x -	 .8f;
			other.gameObject.transform.position = vec;
			other.gameObject.SendMessage("death");
		}
		//other.gameObject.SendMessage("activateShield");
		//Destroy(this.gameObject);
		
	}
	/*
	public void OnCollisionStay2D(Collision2D other )
	{
		if(other.gameObject.tag == "Player"){
			other.gameObject.SendMessage("takeDamage");
			ContactPoint2D contact = other.contacts[0];
			Quaternion rot = Quaternion.FromToRotation(Vector2.up, contact.normal);
			Vector2 explosionPoint = contact.point;
			Instantiate(explosion1,explosionPoint,rot);
		}

	}*/

	public void DestroyLight()
	{
		Destroy (this.gameObject);
	}
}
