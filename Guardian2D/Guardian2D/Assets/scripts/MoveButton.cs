﻿/*/
* Script by Devin Curry
* www.Devination.com
* www.youtube.com/user/curryboy001
* Please like and subscribe if you found my tutorials helpful :D
* Google+ Community: https://plus.google.com/communities/108584850180626452949
* Twitter: https://twitter.com/Devination3D
* Facebook Page: https://www.facebook.com/unity3Dtutorialsbydevin
/*/
using UnityEngine;
using System.Collections;

public class MoveButton : TouchLogicV2 
{
	//[Range(-1,1)]
	public int moveDir;//1=right;-1=left
	PlayerMovement1 player;
	//private Animator animator;

	void Start()
	{
		//animator = GetComponent<Animator> ();
		player = FindObjectOfType<PlayerMovement1>();//This will find our player script, as long as there is only 1 GameObject with "PlayerMovement" on it
		player.animator.SetInteger ("AnimState", 0);

	}

	public override void OnTouchBegan()
	{	//Debug.Log ("move OnTouchBegan");
		//watch this touch for when it ends anywhere so we can slow down the player
		//player.touched = true;
		touch2Watch = currTouch;
	}

	public override void OnTouchMoved()

	{	//player.touched = true;
		//Debug.Log ("move OnTouchMoved");
		player.move (moveDir);
	}
	public override void OnTouchStayed()
	{
		//player.touched = true;
		//Debug.Log ("move OnTouchStayed");
		player.move (moveDir);
	}
	/*
	public override void OnTouchEndedAnywhere()
	{
		Debug.Log ("move OnTouchEndedAnywhere");
		//run this check so other touches ending don't cause player to slow down
		player.animator.SetInteger ("AnimState", 0);
		player.touched = false;
		if(currTouch == touch2Watch)
			player.move(0);//do avoid annoying drift after letting go of button
	}
	*/
	public override void OnTouchEnded()
	{
		//Debug.Log ("move OnTouchEnded");
		player.animator.SetInteger ("AnimState", 0);
		player.touched = false;
	}
}